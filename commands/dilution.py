import re

from base import BaseCommand

DIL_RE = re.compile('(\d+)\+(\d+)\s+(\d+)')

def calculate(comp1, comp2, total):
    tot_cmp = comp1 + comp2
    base = total / float(tot_cmp)
    return round(base * comp1), round(base * comp2)

class DilutionCalculator(BaseCommand):
    command = 'dilution'

    def handle_command(self, command, channel, evt):
        args = self.get_args(command)

        match = DIL_RE.match(args)
        if not match:
            return "Chcesz znać wynik? TO KURWA PODAJ POPRAWNE ZAPYTANIE CHUJU np 1+3 500"
        
        comp1, comp2, total = match.groups()

        comp1, comp2, total = int(comp1), int(comp2), int(total)

        return '%.2f dev + %.2f wody. Mam nadzieję, że pomogłem.' % calculate(comp1, comp2, total)
